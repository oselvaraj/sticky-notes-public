'use strict';

angular.module('notesApp')

// .controller('MenuController', ['$scope', 'menuFactory', 'favoriteFactory', function ($scope, menuFactory, favoriteFactory) {
//
//     $scope.tab = 1;
//     $scope.filtText = '';
//     $scope.showDetails = false;
//     $scope.showFavorites = false;
//     $scope.showMenu = false;
//     $scope.message = "Loading ...";
//
//     menuFactory.query(
//         function (response) {
//             $scope.dishes = response;
//             $scope.showMenu = true;
//
//         },
//         function (response) {
//             $scope.message = "Error: " + response.status + " " + response.statusText;
//         });
//
//     $scope.select = function (setTab) {
//         $scope.tab = setTab;
//
//         if (setTab === 2) {
//             $scope.filtText = "appetizer";
//         } else if (setTab === 3) {
//             $scope.filtText = "mains";
//         } else if (setTab === 4) {
//             $scope.filtText = "dessert";
//         } else {
//             $scope.filtText = "";
//         }
//     };
//
//     $scope.isSelected = function (checkTab) {
//         return ($scope.tab === checkTab);
//     };
//
//     $scope.toggleDetails = function () {
//         $scope.showDetails = !$scope.showDetails;
//     };
//
//     $scope.toggleFavorites = function () {
//         $scope.showFavorites = !$scope.showFavorites;
//     };
//
//     $scope.addToFavorites = function(dishid) {
//         console.log('Add to favorites', dishid);
//         favoriteFactory.save({_id: dishid});
//         $scope.showFavorites = !$scope.showFavorites;
//     };
// }])
//
// .controller('ContactController', ['$scope', 'feedbackFactory', function ($scope, feedbackFactory) {
//
//     $scope.feedback = {
//         mychannel: "",
//         firstName: "",
//         lastName: "",
//         agree: false,
//         email: ""
//     };
//
//     var channels = [{
//         value: "tel",
//         label: "Tel."
//     }, {
//         value: "Email",
//         label: "Email"
//     }];
//
//     $scope.channels = channels;
//     $scope.invalidChannelSelection = false;
//
//     $scope.sendFeedback = function () {
//
//
//         if ($scope.feedback.agree && ($scope.feedback.mychannel == "")) {
//             $scope.invalidChannelSelection = true;
//         } else {
//             $scope.invalidChannelSelection = false;
//             feedbackFactory.save($scope.feedback);
//             $scope.feedback = {
//                 mychannel: "",
//                 firstName: "",
//                 lastName: "",
//                 agree: false,
//                 email: ""
//             };
//             $scope.feedback.mychannel = "";
//             $scope.feedbackForm.$setPristine();
//         }
//     };
// }])
//
// .controller('DishDetailController', ['$scope', '$state', '$stateParams', 'menuFactory', 'commentFactory', function ($scope, $state, $stateParams, menuFactory, commentFactory) {
//
//     $scope.dish = {};
//     $scope.showDish = false;
//     $scope.message = "Loading ...";
//
//     $scope.dish = menuFactory.get({
//             id: $stateParams.id
//         })
//         .$promise.then(
//             function (response) {
//                 $scope.dish = response;
//                 $scope.showDish = true;
//             },
//             function (response) {
//                 $scope.message = "Error: " + response.status + " " + response.statusText;
//             }
//         );
//
//     $scope.mycomment = {
//         rating: 5,
//         comment: ""
//     };
//
//     $scope.submitComment = function () {
//
//         commentFactory.save({id: $stateParams.id}, $scope.mycomment);
//
//         $state.go($state.current, {}, {reload: true});
//
//         $scope.commentForm.$setPristine();
//
//         $scope.mycomment = {
//             rating: 5,
//             comment: ""
//         };
//     }
// }])

// implement the IndexController and About Controller here
//
// .controller('HomeController', ['$scope', 'menuFactory', 'corporateFactory', 'promotionFactory', function ($scope, menuFactory, corporateFactory, promotionFactory) {
//     $scope.showDish = false;
//     $scope.showLeader = false;
//     $scope.showPromotion = false;
//     $scope.message = "Loading ...";
//     var leaders = corporateFactory.query({
//             featured: "true"
//         })
//         .$promise.then(
//             function (response) {
//                 var leaders = response;
//                 $scope.leader = leaders[0];
//                 $scope.showLeader = true;
//             },
//             function (response) {
//                 $scope.message = "Error: " + response.status + " " + response.statusText;
//             }
//         );
//     $scope.dish = menuFactory.query({
//             featured: "true"
//         })
//         .$promise.then(
//             function (response) {
//                 var dishes = response;
//                 $scope.dish = dishes[0];
//                 $scope.showDish = true;
//             },
//             function (response) {
//                 $scope.message = "Error: " + response.status + " " + response.statusText;
//             }
//         );
//     var promotions = promotionFactory.query({
//         featured: "true"
//     })
//     .$promise.then(
//             function (response) {
//                 var promotions = response;
//                 $scope.promotion = promotions[0];
//                 $scope.showPromotion = true;
//             },
//             function (response) {
//                 $scope.message = "Error: " + response.status + " " + response.statusText;
//             }
//         );
// }])
//
// .controller('AboutController', ['$scope', 'corporateFactory', function ($scope, corporateFactory) {
//
//     $scope.leaders = corporateFactory.query();
//
// }])
//
// .controller('FavoriteController', ['$scope', '$state', 'favoriteFactory', function ($scope, $state, favoriteFactory) {
//
//     $scope.tab = 1;
//     $scope.filtText = '';
//     $scope.showDetails = false;
//     $scope.showDelete = false;
//     $scope.showMenu = false;
//     $scope.message = "Loading ...";
//
//     favoriteFactory.query(
//         function (response) {
//             $scope.dishes = response.dishes;
//             $scope.showMenu = true;
//         },
//         function (response) {
//             $scope.message = "Error: " + response.status + " " + response.statusText;
//         });
//
//     $scope.select = function (setTab) {
//         $scope.tab = setTab;
//
//         if (setTab === 2) {
//             $scope.filtText = "appetizer";
//         } else if (setTab === 3) {
//             $scope.filtText = "mains";
//         } else if (setTab === 4) {
//             $scope.filtText = "dessert";
//         } else {
//             $scope.filtText = "";
//         }
//     };
//
//     $scope.isSelected = function (checkTab) {
//         return ($scope.tab === checkTab);
//     };
//
//     $scope.toggleDetails = function () {
//         $scope.showDetails = !$scope.showDetails;
//     };
//
//     $scope.toggleDelete = function () {
//         $scope.showDelete = !$scope.showDelete;
//     };
//
//     $scope.deleteFavorite = function(dishid) {
//         console.log('Delete favorites', dishid);
//         favoriteFactory.delete({id: dishid});
//         $scope.showDelete = !$scope.showDelete;
//         $state.go($state.current, {}, {reload: true});
//     };
// }])
//
.controller('HeaderController', ['$scope', '$state', '$rootScope', 'ngDialog', 'AuthFactory', function ($scope, $state, $rootScope, ngDialog, AuthFactory) {

    $scope.loggedIn = false;
    $scope.username = '';

    if(AuthFactory.isAuthenticated()) {
        $scope.loggedIn = true;
        $scope.username = AuthFactory.getUsername();
    }

    $scope.openLogin = function () {
        ngDialog.open({ template: 'views/login.html', scope: $scope, className: 'ngdialog-theme-default', controller:"LoginController" });
    };

    $scope.logOut = function() {
       AuthFactory.logout();
        $scope.loggedIn = false;
        $scope.username = '';
        $state.go('notesapp');
    };

    $rootScope.$on('login:Successful', function () {
        $scope.loggedIn = AuthFactory.isAuthenticated();
        $scope.username = AuthFactory.getUsername();
    });

    $rootScope.$on('registration:Successful', function () {
        $scope.loggedIn = AuthFactory.isAuthenticated();
        $scope.username = AuthFactory.getUsername();
    });

    $scope.stateis = function(curstate) {
       return $state.is(curstate);
    };

}])

.controller('LoginController', ['$scope', 'ngDialog', '$localStorage', 'AuthFactory', '$state', '$rootScope', function ($scope, ngDialog, $localStorage, AuthFactory, $state, $rootScope) {

  if(AuthFactory.isAuthenticated()) {
    $state.go('notesapp.home');
  }

    $scope.loginData = $localStorage.getObject('userinfo','{}');

    $scope.doLogin = function() {
        if($scope.rememberMe)
           $localStorage.storeObject('userinfo',$scope.loginData);

        AuthFactory.login($scope.loginData);
        //$state.go('notesapp.home');
        // ngDialog.close();

    };

    $scope.openRegister = function () {
      $state.go('notesapp.register');
    };

    $rootScope.$on('login:Successful', function () {
      $state.go('notesapp.home');
    });

}])

.controller('RegisterController', ['$scope', 'ngDialog', '$localStorage', 'AuthFactory', '$state', '$rootScope', function ($scope, ngDialog, $localStorage, AuthFactory, $state, $rootScope) {
console.log("hello");
    $scope.register={};
    $scope.loginData={};

  if(AuthFactory.isAuthenticated()) {
    $state.go('notesapp.home');
  }

    $scope.doRegister = function() {

        console.log('Doing registration', $scope.registration);

        AuthFactory.register($scope.registration);
        // $state.go('notesapp.home');
        // ngDialog.close();

    };


    $scope.openLogin = function () {
      $state.go('notesapp.login');
    };

    $rootScope.$on('registration:Successful', function () {
      $state.go('notesapp.home');
    });
}])


.controller('IndexController', ['$scope', 'notesFactory', 'ngDialog', 'AuthFactory', '$rootScope', '$state', function ($scope, feedbackFactory, ngDialog, AuthFactory, $rootScope, $state) {

  $scope.loggedIn = false;
  $scope.username = '';
  $scope.openLogin = function () {
      ngDialog.open({ template: 'views/login.html', disableAnimation: true, scope: $scope, className: 'ngdialog-theme-default', controller:"LoginController" , showClose: false, closeByDocument: false});
  };

console.log("in home controller");
  if(AuthFactory.isAuthenticated()) {
    console.log("logged in aready");
      $scope.loggedIn = true;
      $scope.username = AuthFactory.getUsername();
      $state.go('notesapp.home');
  }
    else{
          console.log("please login in now");
          $state.go('notesapp.login');
  }

}])

.controller('NotesController', ['$scope', 'notesFactory', 'ngDialog', 'AuthFactory', '$rootScope', '$state', function ($scope, notesFactory, ngDialog, AuthFactory, $rootScope, $state) {

  $scope.loggedIn = false;
  $scope.username = '';

console.log("in home controller");
  if(AuthFactory.isAuthenticated()) {
    console.log("logged in aready");
      $scope.loggedIn = true;
      $scope.username = AuthFactory.getUsername();
  }
    else{
          console.log("please login in now");
          $state.go('notesapp.login');
  }
      function queryNotes(){
      notesFactory.query(
          function (response) {
              $scope.notes = response;
          },
          function (response) {
              $scope.message = "Error: " + response.status + " " + response.statusText;

              console.log($scope.message);

              if(response.status == 401){
                AuthFactory.logout();
                $state.go('notesapp.login');
              }
          });
        }
      queryNotes();

      $scope.createNew = function(){
        notesFactory.save({title: 'New Note', color: 'yellow', text:''})
                .$promise.then(
                    function (response) {
                        queryNotes();
                    },
                    function (response) {
                        $scope.message = "Error: " + response.status + " " + response.statusText;
                        console.log($scope.message);
                    }
                );
      }

      $scope.deleteAll = function(){
        notesFactory.delete({})
                .$promise.then(
                    function (response) {
                        queryNotes();
                    },
                    function (response) {
                        $scope.message = "Error: " + response.status + " " + response.statusText;
                        console.log($scope.message);
                    }
                );
      };

      $scope.openNote = function(note){
        $rootScope.currentNote = note;
        $state.go('notesapp.home.editNote');
      }

      $scope.currentNote = $rootScope.currentNote;

      $scope.closeNote = function(){
        $state.go('notesapp.home');
      }

      $scope.deleteNote = function(){
        notesFactory.delete({ id:$scope.currentNote._id })
        .$promise.then(
            function (response) {
                $state.go('notesapp.home');
            },
            function (response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
                console.log($scope.message);
            }
        );
      }

      $scope.saveNote = function(){
        // $scope.currentNote.color = 'pink';
        console.log($scope.currentNote);
        notesFactory.update({ id:$scope.currentNote._id }, $scope.currentNote)
        .$promise.then(
            function (response) {
                $state.go('notesapp.home');
            },
            function (response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
                console.log($scope.message);
            }
        );
      }

      $scope.changeBackground = function(color){
        $scope.currentNote.color = color;
      }

      // if($scope.currentNote){
      //   $scope.newTitle = $scope.currentNote.title;
      //   $scope.newText = $scope.currentNote.text;
      //   console.log($scope.newTitle);
      // }

      $scope.setColor = function(color, type) {
        var reducedColor = '';
        var highColor = '';
        switch(color){
          case 'yellow':
            reducedColor = '#ffffcc';
            highColor = '#ffff33';
            break;
          case 'green':
          reducedColor = '#ccffcc';
          highColor = '#33ff33';
          break;
          case 'blue':
          reducedColor = '#ccffff';
          highColor = '#33ffff';
          break;
          case 'pink':
          reducedColor = '#ffccff';
          highColor = '#ff33ff';
          break;
        }
        if(type == 'heading')
              return  'background-color: ' + color + ' !important; background-image: linear-gradient(to bottom,'+ reducedColor +' 0,'+highColor+' 100%) !important; border: none';
        else {
          return  'background-color: ' + reducedColor + ' !important; border: none';
        }
       };
}])

;
