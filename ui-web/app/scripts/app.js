'use strict';

angular.module('notesApp', ['ui.router','ngResource','ngDialog', 'ngSanitize'])
.config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
        .state('notesapp', {
            url:'/',
            views: {
                'content': {
                    template : '<ui-view/>',
                    controller  : 'IndexController'
                }
                // ,
                // 'header':{
                //   templateUrl : 'views/header.html',
                //   //controller  : 'HomeController'
                // },
                // 'notes':{
                //   templateUrl : 'views/header.html',
                //   //controller  : 'HomeController'
                // }
              }
            })
            .state('notesapp.login', {
                url:'login',
                views: {
                    'content@': {
                        templateUrl : 'views/login.html',
                        controller  : 'LoginController'
                    }
                }
              })
              .state('notesapp.register', {
                  url:'register',
                  views: {
                      'content@': {
                          templateUrl : 'views/register.html',
                          controller  : 'RegisterController'
                      }
                  }
                })
              .state('notesapp.home', {
                  url: 'mynotes',
                  views: {
                      'content@': {
                          templateUrl : 'views/home.html'
                      },
                      'header@notesapp.home': {
                          templateUrl : 'views/header.html',
                          controller  : 'HeaderController'
                      },
                      'notes@notesapp.home': {
                          templateUrl : 'views/notes.html',
                          controller  : 'NotesController'
                      }
                  }
                })
                .state('notesapp.home.editNote', {
                    views: {
                        'notes@notesapp.home': {
                            templateUrl : 'views/current-note.html',
                            controller  : 'NotesController'
                        }
                    }
                  })
            // route for the home page
            .state('app', {
                url:'/sample',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller  : 'HeaderController'
                    },
                    'content': {
                        templateUrl : 'views/home.html',
                        controller  : 'HomeController'
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                }

            })

            // route for the aboutus page
            .state('app.aboutus', {
                url:'aboutus',
                views: {
                    'content@': {
                        templateUrl : 'views/aboutus.html',
                        controller  : 'AboutController'
                    }
                }
            })

            // route for the contactus page
            .state('app.contactus', {
                url:'contactus',
                views: {
                    'content@': {
                        templateUrl : 'views/contactus.html',
                        controller  : 'ContactController'
                    }
                }
            })

            // route for the menu page
            .state('app.menu', {
                url: 'menu',
                views: {
                    'content@': {
                        templateUrl : 'views/menu.html',
                        controller  : 'MenuController'
                    }
                }
            })

            // route for the dishdetail page
            .state('app.dishdetails', {
                url: 'menu/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/dishdetail.html',
                        controller  : 'DishDetailController'
                   }
                }
            })

            // route for the dishdetail page
            .state('app.favorites', {
                url: 'favorites',
                views: {
                    'content@': {
                        templateUrl : 'views/favorites.html',
                        controller  : 'FavoriteController'
                   }
                }
            });



        $urlRouterProvider.otherwise('/');
    })

    .directive('contenteditable', function () {
         return {
             restrict: 'A', // only activate on element attribute
             require: '?ngModel', // get a hold of NgModelController
             link: function (scope, element, attrs, ngModel) {
                 if (!ngModel) return; // do nothing if no ng-model

                 // Specify how UI should be updated
                 ngModel.$render = function () {
                     element.html(ngModel.$viewValue || '');
                 };

                 // Listen for change events to enable binding
                 element.on('blur keyup change', function () {
                     scope.$apply(readViewText);
                 });

                 // No need to initialize, AngularJS will initialize the text based on ng-model attribute

                 // Write data to the model
                 function readViewText() {
                     var html = element.html();
                     // When we clear the content editable the browser leaves a <br> behind
                     // If strip-br attribute is provided then we strip this out
                     if (attrs.stripBr && html == '<br>') {
                         html = '';
                     }
                     ngModel.$setViewValue(html);
                 }
             }
         };
     });

    // .directive('contenteditable', ['$sce', function($sce) {
    //   return {
    //     restrict: 'A', // only activate on element attribute
    //     require: '?ngModel', // get a hold of NgModelController
    //     link: function(scope, element, attrs, ngModel) {
    //       if (!ngModel) return; // do nothing if no ng-model
    //
    //       // Specify how UI should be updated
    //       ngModel.$render = function() {
    //         element.html($sce.getTrustedHtml(ngModel.$viewValue || ''));
    //       };
    //
    //       // Listen for change events to enable binding
    //       element.on('blur keyup change', function() {
    //         scope.$evalAsync(read);
    //       });
    //       read(); // initialize
    //
    //       // Write data to the model
    //       function read() {
    //         var html = element.html();
    //         // When we clear the content editable the browser leaves a <br> behind
    //         // If strip-br attribute is provided then we strip this out
    //         if (attrs.stripBr && html === '<br>') {
    //           html = '';
    //         }
    //         ngModel.$setViewValue(html);
    //       }
    //     }
    //   };
    // }]);
