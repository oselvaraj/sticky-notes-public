"use strict";
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Favorites = require('../models/favorites');

var Verify = require('./verify');

var favoritesRouter = express.Router();
favoritesRouter.use(bodyParser.json());

favoritesRouter.route('/')
.get(Verify.verifyOrdinaryUser, function (req, res, next) {
    Favorites.find({postedBy:req.decoded._doc._id})
        .populate('postedBy')
        .populate('dishes')
        .exec(function (err, favorites) {
        if (err) throw err;
        res.json(favorites);
    });
})

.post(Verify.verifyOrdinaryUser, function (req, res, next) {

  Favorites.find({postedBy:req.decoded._doc._id})
      .exec(function (err, favorites) {
      if (err) throw err;

      if(favorites.length == 0){
        console.log(JSON.stringify({postedBy:req.decoded._doc._id, dishes:[req.body._id]}));

        Favorites.create({postedBy:req.decoded._doc._id, dishes:[req.body._id]}, function (err, favorites) {
            if (err) {console.log(err); throw err;}
            console.log('Favorites newly created!');

            // res.writeHead(200, {
            //     'Content-Type': 'text/plain'
            // });

            res.json(favorites);
        });
      }
      else{

        for (let idx=favorites[0].dishes.length-1; idx>=0; idx--) {
          if (favorites[0].dishes[idx] == req.body._id) {
            return res.json(favorites);
              //favorites[0].dishes.splice(idx, 1);
              // break;       //<-- Uncomment  if only the first term has to be removed
          }
        }

            console.log(JSON.stringify(favorites));
            favorites[0].dishes.push(req.body._id);
            favorites[0].save(function (err, favorites) {
                if (err) throw err;
                console.log('Added Dish to Existing Favorites!');
                res.json(favorites);
            });
      }
  });

})

.delete(Verify.verifyOrdinaryUser,  function (req, res, next) {
    Favorites.remove({postedBy:req.decoded._doc._id}, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
});

favoritesRouter.route('/:dishId')

.delete(Verify.verifyOrdinaryUser,  function (req, res, next) {
  Favorites.find({postedBy:req.decoded._doc._id}, function (err, favorites) {
    console.log(JSON.stringify(favorites[0].dishes));
    console.log(req.params.dishId);
      //favorites[0].dishes.id(req.params.dishId).remove();
      for (let idx=favorites[0].dishes.length-1; idx>=0; idx--) {
        if (favorites[0].dishes[idx] == req.params.dishId) {
            favorites[0].dishes.splice(idx, 1);
            // break;       //<-- Uncomment  if only the first term has to be removed
        }
    }
    console.log(JSON.stringify(favorites[0].dishes));
      favorites[0].save(function (err, resp) {
          if (err) throw err;
          res.json(resp);
      });
  });
});


module.exports = favoritesRouter;

/*
var express = require('express');
var bodyParser = require('body-parser');

var Favorites = require('../models/favorites');
var Verify = require('./verify');

var favoriteRouter = express.Router();
favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
.all(Verify.verifyOrdinaryUser)
.get(function (req, res, next) {

    var userId = req.decoded._doc._id;

    Favorites.find({ postedBy: userId })
      .populate('postedBy dishes')
      .exec(function (err, dish) {
        if (err) throw err;

        res.json(dish);
    });
})

.post(function (req, res, next) {
    var userId = req.decoded._doc._id;
    var dishId = req.body._id;

    console.log('userId = ' + userId + ', dishId = ' + dishId);
    Favorites.update({ postedBy: userId },
      { $push: { dishes: dishId } },
      { upsert: true },
      function (err, data) {
        if (err) throw err;

        res.json(data);
    });
})

.delete(function (req, res, next) {

    var userId = req.decoded._doc._id;

    Favorites.remove({ postedBy: userId }, function (err, resp) {
      if (err) throw err;

      res.json(resp);
    })
});

favoriteRouter.route('/:dishId')

  .delete(Verify.verifyOrdinaryUser, function (req, res, next) {
    var dishId = req.params.dishId;
    var userId = req.decoded._doc._id;

    Favorites.update({ postedBy: userId },
      { $pull: { dishes: dishId } },
      { upsert: true },
      function (err, data) {
        if (err) throw err;

        res.json(data);
    });

});

module.exports = favoriteRouter;*/
