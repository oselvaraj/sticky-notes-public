"use strict";
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Notes = require('../models/notes');

var Verify = require('./verify');

var notesRouter = express.Router();
notesRouter.use(bodyParser.json());

notesRouter.route('/')
.get(Verify.verifyOrdinaryUser, function (req, res, next) {
    Notes.find({postedBy:req.decoded._doc._id})
        .populate('postedBy')
        .exec(function (err, notes) {
        if (err) throw err;
        res.json(notes);
    });
})

.post(Verify.verifyOrdinaryUser, function (req, res, next) {
    var noteContent = req.body;
    noteContent.postedBy = req.decoded._doc._id;
    Notes.create(noteContent, function (err, note) {
        if (err) throw err;
        console.log('Note created!');
        var id = note._id;
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });

        res.end('Added the note with id: ' + id);
    });
})

.delete(Verify.verifyOrdinaryUser,  function (req, res, next) {
    Notes.remove({postedBy:req.decoded._doc._id}, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
});

notesRouter.route('/:noteId')
.get(Verify.verifyOrdinaryUser, function (req, res, next) {
  Notes.find({postedBy:req.decoded._doc._id, _id: req.params.noteId})
      .populate('postedBy')
      .exec(function (err, notes) {
          if (err) throw err;
          res.json(notes);
      });

})
.put(Verify.verifyOrdinaryUser,  function (req, res, next) {
  var noteContent = req.body;
  noteContent.postedBy = req.decoded._doc._id;
    Notes.findByIdAndUpdate({postedBy:req.decoded._doc._id, _id: req.params.noteId}, {
        $set: noteContent
    }, {
        new: true
    }, function (err, note) {
        if (err) throw err;
        res.json(note);
    });
})

.delete(Verify.verifyOrdinaryUser,  function (req, res, next) {
        Notes.findOneAndRemove({postedBy:req.decoded._doc._id, _id: req.params.noteId}, function (err, resp) {
        if (err) throw err;
        res.json(resp);
    });
});


module.exports = notesRouter;

/*
var express = require('express');
var bodyParser = require('body-parser');

var Notes = require('../models/notes');
var Verify = require('./verify');

var noteRouter = express.Router();
noteRouter.use(bodyParser.json());

noteRouter.route('/')
.all(Verify.verifyOrdinaryUser)
.get(function (req, res, next) {

    var userId = req.decoded._doc._id;

    Notes.find({ postedBy: userId })
      .populate('postedBy dishes')
      .exec(function (err, dish) {
        if (err) throw err;

        res.json(dish);
    });
})

.post(function (req, res, next) {
    var userId = req.decoded._doc._id;
    var dishId = req.body._id;

    console.log('userId = ' + userId + ', dishId = ' + dishId);
    Notes.update({ postedBy: userId },
      { $push: { dishes: dishId } },
      { upsert: true },
      function (err, data) {
        if (err) throw err;

        res.json(data);
    });
})

.delete(function (req, res, next) {

    var userId = req.decoded._doc._id;

    Notes.remove({ postedBy: userId }, function (err, resp) {
      if (err) throw err;

      res.json(resp);
    })
});

noteRouter.route('/:dishId')

  .delete(Verify.verifyOrdinaryUser, function (req, res, next) {
    var dishId = req.params.dishId;
    var userId = req.decoded._doc._id;

    Notes.update({ postedBy: userId },
      { $pull: { dishes: dishId } },
      { upsert: true },
      function (err, data) {
        if (err) throw err;

        res.json(data);
    });

});

module.exports = noteRouter;*/
