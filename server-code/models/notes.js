var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var noteSchema = new Schema({
    postedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        //unique: true
    },
    title: {
        type: String//,
        // required: true,
        //unique: true
    },
    text: {
        type: String//,
        // required: true
    },
    color: {
        type: String,
        required: true
    }
  }, {
      timestamps: true
});

// the schema is useless so far
// we need to create a model using it
var Notes = mongoose.model('Note', noteSchema);

// make this available to our Node applications
module.exports = Notes;
